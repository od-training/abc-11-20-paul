import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Video } from '../app.types';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  fullVideoList = this.http.get<Video[]>('http://localhost:4300/api/videos');
  searchTermControl = new FormControl();

  allTerms = this.activatedRoute.queryParamMap.pipe(
    map(paramMap => paramMap.get('searchTerm')),
    startWith<string>('')
  );

  filteredVideoList = combineLatest([this.fullVideoList, this.allTerms]).pipe(
    map(([fullList, searchTerm]) =>
       fullList.filter(video => video.title.includes(searchTerm))
    )
  );

  userSelectedVideo = new BehaviorSubject<Video | undefined>(undefined);

  selectedVideo = combineLatest([this.filteredVideoList, this.userSelectedVideo]).pipe(
    map(([filteredVideoList, userSelectedVideo]) => {
      return filteredVideoList
        .find(video => video.id === userSelectedVideo?.id) || filteredVideoList[0];
    })
  );

  constructor(private http: HttpClient, router: Router, private activatedRoute: ActivatedRoute) {
    this.searchTermControl.valueChanges
      .subscribe(searchTerm => router.navigate([], { queryParams: {searchTerm}}));
   }

  setSelectedVideo(video: Video): void{
    this.userSelectedVideo.next(video);
  }

}
