import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import { Video } from '../../app.types';
import { DashboardService } from '../dashboard.service';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss']
})
export class VideoDashboardComponent {
  videos: Observable<Video[]> = this.dashboardService.filteredVideoList;
  selectedVideo = this.dashboardService.selectedVideo;

  constructor(private dashboardService: DashboardService){}

  setSelectedVideo(video: Video): void {
    this.dashboardService.setSelectedVideo(video);
  }
}
