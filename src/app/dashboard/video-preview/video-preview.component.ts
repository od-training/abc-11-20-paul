import { Component, Input } from '@angular/core';
import { Video } from '../../app.types';

@Component({
  selector: 'app-video-preview',
  templateUrl: './video-preview.component.html',
  styleUrls: ['./video-preview.component.scss']
})
export class VideoPreviewComponent {
  @Input() video: Video | undefined;
}
