import { Component, Input } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

import { Video } from '../../app.types';

const URLPREFIX = 'https://www.youtube.com/embed/';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent {
 // Using an input setter is much more concise than
  // the use of ngOnChanges
  @Input() set video(value: Video | undefined) {
    if (value) {
      // iframe src attributes are a potential
      // source of attack. Tell Angular we have vetted the URL as
      // safe to use. (You would normally check the URL before passing it
      // on )

      /// App specific code for verifying that the url is in fact safe to use
      /// Goes here
      /// TODO:
      this.videoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(
        URLPREFIX + '/' + value.id
      );

    }
  }

  videoUrl: SafeUrl | undefined;

  constructor(private domSanitizer: DomSanitizer) {}

}
