import { Component } from '@angular/core';
import { DashboardService } from '../dashboard.service';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.scss']
})
export class StatFiltersComponent {
  searchTerm = this.dashboardService.searchTermControl;

  constructor(private dashboardService: DashboardService){}

}
