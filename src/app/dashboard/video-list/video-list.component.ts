import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Video } from '../../app.types';


@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent {
  @Input() videos: Video[] = [];
  @Input() selectedVideo: Video | undefined;
  @Output() selectVideo = new EventEmitter<Video>();

  pickVideo(video: Video): void{
    this.selectVideo.emit(video);
  }
}
